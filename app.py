import requests
import time
import os

from flask import Flask
from flask import request, Response
from loguru import logger

app = Flask(__name__)
app.secret_key = os.environ['SECRET_KEY']

def set_status(status, emoji, timeEnd):
    slack_token = os.environ['SLACK_TOKEN']
    url = os.environ['SLACK_URL']
    header = {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": f"Bearer {slack_token}"
    }
    payload = {
        "profile": {
            "status_text": status,
            "status_emoji": emoji,
            "status_expiration": timeEnd
        }
    }
    resp = requests.post(url, json=payload, headers=header)
    if resp.status_code != 200:
        logger.error(f"Slack request failed with status {resp.status_code}.")
        logger.error(resp.content)


@app.route("/", methods=['POST'])
def main():
    data = request.get_json()
    if data['eventType'] == 'timerStart':
        set_status(
            f'Working on {data["name"]} for {data["minutes"]} minutes', 
            ':thinking_face:', 
            time.time() + data['minutes'] * 60
        )
    elif data['eventType'] == 'timerStop':
        set_status('', '', 0)
    return Response(response={'detail': 'OK: successsfully registered event'}, status=200)
