import sys
import requests
from loguru import logger



def main(args):
    try:
        api_key = args[1]
        target_url = args[2]
        event_type = args[3]
    except IndexError:
        logger.error('Usage: python register.py <api_key> <target_url> <event_type>')
        return

    url = f'https://my.pomodoneapp.com/integration/authorize/?integration=gsheet&params[api_key]={api_key}'

    payload = {
        'subscription_url': 'PomoDone Slack Bridge',
        'target_url': target_url,
        'event': event_type
    }

    response = requests.post(url, json=payload)

    if response.status_code == 200:
        logger.success('Web hook registered successfully')
    else:
        logger.error(f'Registration request returned with status code {response.status_code}. '
                      'Check that your API key is correct and that you are using a valid event type.')

main(sys.argv)
