# Warning

This app is old and deprecated. PomoDoneApp has since changed their API, so the app does not work in it's current state. I instead forked this repo and made my own app called [Pomodex](https://gitlab.com/fizzizist/pomodex). I am keeping this here just to showcase it in case someone needs the code for a similar working setup. 

# PomoDoneApp Slack Bridge

A lightweight Flask app for using PomoDoneApp web hooks to change your slack status

## Installation

### Register PomoDoneApp Hooks

Before the app can work, you need to register the web hooks using the `register.py` command:
```bash
python register.py <api_token> <target_url> <event_type>
```
where `api_token` is the API token that you get from your PomoDoneApp settings, `target_url` is the URL where you are hosting this app, and `event_type` is the type of event. Right now only two event types are supports: `timerStop` and `timerStart`.

### Environment Setup
You will need a `.env` file with three variables: 
```bash
SECRET_KEY=secret
SLACK_TOKEN=xoxp-rest-of-token
SLACK_URL=https://myslack.slack.com/api/users.profile.set
```
The secret key is used by Flask, and the slack information is used to change your slack status. This assumes that you have already
set up a slack bot and have a user slack token beginning in `xoxp`.

### Compose
Once the above are set up, it should be as easy as running `docker-compose up -d --build`.

## Removing Web Hooks
If you are no longer using the web hooks, or want to replace them with new ones, you can use the `remove_web_hook.py` command to remove them. You just need to supply the `target_url` that you supplied to register the hook, and it will wipe all event hooks for that url.
