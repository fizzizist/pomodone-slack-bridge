import sys
import requests
from loguru import logger



def main(args):
    try:
        api_key = args[1]
        target_url = args[2]
    except IndexError:
        logger.error('Usage: python register.py <api_key> <target_url>')
        return

    url = f'https://my.pomodoneapp.com/integration/remove/?integration=gsheet&params[api_key]={api_key}'

    payload = {
        'target_url': target_url,
    }

    response = requests.post(url, json=payload)

    if response.status_code == 200:
        logger.success('Web hook removed successfully.')
    else:
        logger.error(f'Removal request returned with status code {response.status_code}. '
                      'Check that your API key is correct and that you are using a valid event type.')

main(sys.argv)
