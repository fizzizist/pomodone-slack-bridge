FROM python:3.10-bullseye as builder

MAINTAINER Fizzizist "pvlasveld@protonmail.com"

COPY requirements.in /app/requirements.in

WORKDIR /app

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN pip install pip-tools \
  && pip-compile \
  && pip wheel --no-cache-dir --no-deps --wheel-dir /app/wheels -r requirements.txt

FROM python:3.10-bullseye

RUN mkdir -p {/home/pomo,/home/pomo/app} \
  && groupadd pomo \
  && useradd pomo -s /bin/bash -g pomo

ENV HOME=/home/pomo
ENV APP_HOME=/home/pomo/app
WORKDIR $APP_HOME

COPY --from=builder /app/wheels /wheels
COPY --from=builder /app/requirements.txt .
RUN pip install --no-cache /wheels/*

COPY . $APP_HOME

RUN chown -R pomo:pomo $APP_HOME

USER pomo
